<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>

	<title>Grid Layout Generator</title>
	<style type='text/css'>

	.wrapper {
		position: relative;
		float: left;
		left: 0px;
		margin-bottom: 10px;
		background-color: #cccccc
	}
 
	.left1 {
		position: relative;
		float: left;
		left: 5px;
		width: 237px;
		height: 237px;
		background-color: #333333
	}

	.left2 {
		position: relative;
		float: left;
		left: 15px;
		width: 237px;
		height: 237px;
		background-color: #333333
	}

	.left3 {
		position: relative;
		float: left;
		left: 25px;
		width: 237px;
		height: 237px;
		background-color: #333333
	}

	.left4 {
		position: relative;
		float: left;
		left: 35px;
		width: 237px;
		height: 237px;
		background-color: #333333
	}

	.left5 {
		position: relative;
		float: left;
		left: 45px;
		width: 237px;
		height: 237px;
		background-color: #333333
	}

	.left5laranja{

		position: relative;
		float: left;
		left: 45px;
		width: 237px;
		height: 237px;
		background-color: #fda929;
	}

	body {
		border-width: 0px;
		padding: 0px;
		margin: 0px;
		font-size: 90%;
		color: #FFFFFF;
		text-align: center;
		background-color: #e7e7de;
		font-family: 'Noto Sans', sans-serif;
	}
	</style>
</head>
<body>
	<div class="wrapper">
		<div class="left1">
			<br><br /></br>
			<table width="100%" align="center" valign='middle'>
				<tr>
					<td><div id='clock_dt'>

					</div></td>
				</tr>
				<tr>
					<td><h1>
						<div id='clock_tm'>

						</div>
					</h1></td>
				</tr>
			</table>
		</div>


		<div class="left3" id='sensor_0'></div>

		<div class="left4"  id='sensor_1'></div>

		<div class="left5laranja">

			<br><br /></br>
			<table width="100%" align="center" valign='middle'>
				<tr>
					<td>Você está em</td>
				</tr>
				<tr>
					<td><h1 > <div id='ambiente'> </div></h1></td>
			</tr>
		</table>

	</div>

</div>
<div class="wrapper">
	<div class="left1"  id='sensor_2'></div>

	<div class="left2  " id='sensor_3'></div>

	<div class="left3"></div>

	<div class="left4"></div>

	<div class="left5"></div>

</div>
</body>


<script type="text/javascript">

function Ajax() {
    this.send = function (type, url, data, back) {
        var ajaxObj = new XMLHttpRequest();
        ajaxObj.open(type, url, true);
        ajaxObj.onreadystatechange = function () {
            if (ajaxObj.readyState == 4 && ajaxObj.status == 200) {
                if (typeof back !== 'undefined')
                    back.call(this, ajaxObj.responseText);

            }
        }
        if (typeof data == 'undefined')
            ajaxObj.send();
        else {
            ajaxObj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxObj.send(data);
        }

    }
}

//StartCode \\
var dt = new Array();
var clockID = 0;
var IDTela = 0 ;
var cl_tf = '0';
var cl_df = '0';

var req = new Ajax();

function UpdateClock()
{
	if(clockID)
	clearTimeout(clockID);

	var tDate = new Date();
	dt[0] = tDate.getFullYear();
	dt[1] = pad(tDate.getMonth()+1);
	dt[2] = pad(tDate.getDate());
	dt[3] = pad(tDate.getHours());
	dt[4] = pad(tDate.getMinutes());
	dt[5] = pad(tDate.getSeconds());
	dt_obj = document.getElementById('clock_dt');  // Este é o identificador da chamada "DATA"
	tm_obj = document.getElementById('clock_tm');  // Este é o identificador da chamada "HORA"
	dt_obj.innerHTML = formatDate();
	tm_obj.innerHTML = formatTime();
	clockID = setTimeout("UpdateClock()", 1000);
}


function pad(val)
{
	if(val <= 9)
	{
		val.toString();
		val = "0" + val;
	}

	return val;
}

function formatDate()
{
	var d;

	d = cl_df.replace(/Y/,dt[0]);
	d = d.replace(/m/,dt[1]);
	d = d.replace(/d/,dt[2]);

	return d;
}

function formatTime()
{
	var t;

	t = cl_tf.replace(/H/,dt[3]);
	t = t.replace(/i/,dt[4]);
	t = t.replace(/s/,dt[5]);

	return t;
}


function StartClock(df,tf)
{
	cl_df = df;
	cl_tf = tf;

	clockID = setTimeout("UpdateClock()", 500);
}

function KillClock()
{
	if(clockID)
	{
		clearTimeout(clockID);
		clockID  = 0;
	}
}

function setCookie(name,val,first)
{
	if(first)
	document.cookie = name + "=" + val;
	else
	document.cookie += name + "=" + val;
}




function UpdateTela()
{
	var i =0 ;
	if(IDTela)
	clearTimeout(IDTela);


		req.send('POST', 'ambientes/index.php', '', function (data) {
		 var retornoSensores = JSON.parse(data);

		document.getElementById("ambiente").innerHTML = retornoSensores.desc_ambiente ;
		total_sensores =  (retornoSensores.sensores.length) - 1 ;
		for(i=0; i <= total_sensores ; i++){
			listagemSensores = (retornoSensores.sensores[i]);
			sensoresLista = (listagemSensores.length)-1;
			strPrevisao = "<table width=\"100%\" align=\"center\" valign='middle'>";
			for(k =0 ; k <= sensoresLista ; k++){
				sensorAtual = listagemSensores[k];
				if(sensorAtual.tipo == 'temperatura'){
					strPrevisao = strPrevisao + '<tr>';
					strPrevisao = strPrevisao + '<td>'+ sensorAtual.descricao ;
					strPrevisao = strPrevisao + '</td>';
					strPrevisao = strPrevisao + '</tr>';
					strPrevisao = strPrevisao + '<tr>';
					strPrevisao = strPrevisao + '<tr>';
					strPrevisao = strPrevisao + '<td>Interna (T) '+ sensorAtual.temperatura + 'ºC  / (H) ' + sensorAtual.humidade+' %';
					strPrevisao = strPrevisao + '</td>';
					strPrevisao = strPrevisao + '</tr>';
					strPrevisao = strPrevisao + '<tr>';
					strPrevisao = strPrevisao + '<td>Sensação térmica (T) '+ sensorAtual.sensacao + 'ºC ';
					strPrevisao = strPrevisao + '</td>';
					strPrevisao = strPrevisao + '</tr>';
				}else if(sensorAtual.tipo == 'interruptor'){
					strPrevisao = strPrevisao + '<tr>';
					strPrevisao = strPrevisao + '<td>'+ '('+(k+1)+') ' + sensorAtual.descricao  ;
					strPrevisao = strPrevisao + '</td>';
					strPrevisao = strPrevisao + '</tr>';
					strPrevisao = strPrevisao + '<tr>';
					if(sensorAtual.status == '1'){
						sensorStatus = 'Ligado';
					}else{
						sensorStatus = 'Desligado';						}
						strPrevisao = strPrevisao + '<td>Estado atual  '+ sensorStatus;
						strPrevisao = strPrevisao + '</td>';
						strPrevisao = strPrevisao + '</tr>';
					}
				}
				strPrevisao = strPrevisao + '</table>';
				document.getElementById("sensor_"+(i)).innerHTML  = strPrevisao ;
			}
			retornoSensores = null;
			strPrevisao = null;
			sensorAtual = null
			IDTela = setTimeout("UpdateTela()", 10000);
		});

	}

	function previsaodoTempo(cidade,divPrevisao){

			req.send('POST', 'http://developers.agenciaideias.com.br/tempo/json/'+cidade, '', function (data) {
				var retornoPrevisao = JSON.parse(data);
			strPrevisao = "<table width=\"100%\" align=\"center\" valign='middle'>";
			strPrevisao = strPrevisao + '<tr>';
			strPrevisao = strPrevisao + '<td>'+ '<img src="'+retornoPrevisao.agora.imagem+'">' ;
			strPrevisao = strPrevisao + '</td>';
			strPrevisao = strPrevisao + '</tr>';
			strPrevisao = strPrevisao + '<tr>';
			strPrevisao = strPrevisao + '<td>'+ retornoPrevisao.cidade ;
			strPrevisao = strPrevisao + '</td>';
			strPrevisao = strPrevisao + '</tr>';
			strPrevisao = strPrevisao + '<tr>';
			strPrevisao = strPrevisao + '<td>'+ retornoPrevisao.agora.data_hora ;
			strPrevisao = strPrevisao + '</td>';
			strPrevisao = strPrevisao + '</tr>';
			strPrevisao = strPrevisao + '<tr>';
			strPrevisao = strPrevisao + '<td>'+ retornoPrevisao.agora.descricao ;
			strPrevisao = strPrevisao + '</td>';
			strPrevisao = strPrevisao + '</tr>';
			strPrevisao = strPrevisao + '<tr>';
			strPrevisao = strPrevisao + '<td>Previsão: (T) '+ retornoPrevisao.agora.temperatura + 'ºC  / (H) ' + retornoPrevisao.agora.umidade;
			strPrevisao = strPrevisao + '</td>';
			strPrevisao = strPrevisao + '</tr>';
			strPrevisao = strPrevisao + '</table>';
			document.getElementById(divPrevisao).innerHTML = strPrevisao;
		});

	}
	//EndCode \\
	StartClock('d/m/Y','H:i:s');
	previsaodoTempo('Santo-Andre-SP','previsaoDoTempo');
	UpdateTela();
	</script>
	</html>
