<?php

class Mosquitto
{
    private $login_mqtt;
    private $senha_mqtt;
    private $url_mqtt;
    private $mqtt_client;
    /**
     * Get the value of Login Mqtt
     *
     * @return mixed
     */
    public function getLoginMqtt()
    {
        return $this->login_mqtt;
    }

    /**
     * Set the value of Login Mqtt
     *
     * @param mixed login_mqtt
     *
     * @return self
     */
    public function setLoginMqtt($login_mqtt)
    {
        $this->login_mqtt = $login_mqtt;

        return $this;
    }

    /**
     * Get the value of Senha Mqtt
     *
     * @return mixed
     */
    public function getSenhaMqtt()
    {
        return $this->senha_mqtt;
    }

    /**
     * Set the value of Senha Mqtt
     *
     * @param mixed senha_mqtt
     *
     * @return self
     */
    public function setSenhaMqtt($senha_mqtt)
    {
        $this->senha_mqtt = $senha_mqtt;

        return $this;
    }

    /**
     * Get the value of Url Mqtt
     *
     * @return mixed
     */
    public function getUrlMqtt()
    {
        return $this->url_mqtt;
    }

    /**
     * Set the value of Url Mqtt
     *
     * @param mixed url_mqtt
     *
     * @return self
     */
    public function setUrlMqtt($url_mqtt)
    {
        $this->url_mqtt = $url_mqtt;

        return $this;
    }
    /**
     * Get the value of Mqtt Client
     *
     * @return mixed
     */
    public function getMqttClient()
    {
        return $this->mqtt_client;
    }

    /**
     * Set the value of Mqtt Client
     *
     * @param mixed mqtt_client
     *
     * @return self
     */
    public function setMqttClient($mqtt_client)
    {
        $this->mqtt_client = $mqtt_client;

        return $this;
    }





    public function __construct($json_config)
    {
        $this->setLoginMqtt($json_config->login_mqtt);
        $this->setSenhaMqtt($json_config->senha_mqtt);
        $this->setUrlMqtt($json_config->url_mqtt);
    }

    public function connect()
    {
        $client = new Mosquitto\Client();
        $client->connect($this->getUrlMqtt(), 1883, 5);
        $client->setCredentials($this->getLoginMqtt(), $this->getSenhaMqtt());
        $client->subscribe('#', 0);
        $this->setMqttClient($client);
    }


    public function sendData($topic, $data)
    {
        $this->getMqttClient()->publish($topic, $data);
        $this->getMqttClient()->disconnect();
        $this->setMqttClient(null);
    }

    public function getData($topic=null)
    {
        $this->getMqttClient()->onMessage(
              function ($message) {
                  $this->mosquitto_data[$message->topic] = $message->payload;
              }
          );

        for ($i = 0; $i < 5; $i++) {
            $this->getMqttClient()->loop();
        }

        if ($topic) {
            return $this->mosquitto_data[$topic];
        } else {
            return $this->mosquitto_data;
        }
    }
}
