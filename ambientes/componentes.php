<?php
require_once '../vendor/autoload.php';

Twig_Autoloader::register();
$mosquitto = new mosquitto_raspibr($json_config);

$loader = new Twig_Loader_Filesystem('./');
$twig = new Twig_Environment($loader, array('debug' => true));
$template = $twig->load('componentes.html');

$descricao = base64_decode($_GET['desc']);
$diretorio = $_GET['amb'];

$json_ambiente = json_decode(file_get_contents($diretorio.'/'.'dispositivos.json'));

foreach($json_ambiente->equipamentos as $idx1=>&$equipamento){


 foreach($equipamento->sensores as $idx2=>&$sensor){

   if(trim($execution) == 'On'){
     $sensor->state = 'On';
   }else{
     $sensor->state = 'Off';
   }
 }

}


echo $template->render(

         array(
            'ambiente' => $descricao,
            'componentes'=> $json_ambiente
            )

        );
