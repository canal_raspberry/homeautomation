var dt = new Array();
var clockID = 0;
var IDTela = 0;
var cl_tf = '0';
var cl_df = '0';

var position;

function pausecomp(millis) {
    var date = new Date();
    var curDate = null;
    do {
        curDate = new Date();
    }
    while (curDate - date < millis);
}


function pad(val) {
    if (val <= 9) {
        val.toString();
        val = "0" + val;
    }

    return val;
}

function formatDate() {
    var d;
    d = cl_df.replace(/Y/, dt[0]);
    d = d.replace(/m/, dt[1]);
    d = d.replace(/d/, dt[2]);
    return d;
}

function formatTime() {
    var t;
    t = cl_tf.replace(/H/, dt[3]);
    t = t.replace(/i/, dt[4]);
    t = t.replace(/s/, dt[5]);
    return t;
}


function UpdateClock() {
    if (clockID)
        clearTimeout(clockID);
    var tDate = new Date();
    dt[0] = tDate.getFullYear();
    dt[1] = pad(tDate.getMonth() + 1);
    dt[2] = pad(tDate.getDate());
    dt[3] = pad(tDate.getHours());
    dt[4] = pad(tDate.getMinutes());
    dt[5] = pad(tDate.getSeconds());
    dt_obj = document.getElementById('data-main'); // Este é o identificador da chamada "DATA"
    tm_obj = document.getElementById('clock_tm'); // Este é o identificador da chamada "HORA"
    dt_obj.innerHTML = formatDate();
    tm_obj.innerHTML = "<strong>" + formatTime() + "</strong>";
    clockID = setTimeout("UpdateClock()", 1000);
}

function StartClock(df, tf) {
    cl_df = df;
    cl_tf = tf;
    clockID = setTimeout("UpdateClock()", 500);
}

function KillClock() {
    if (clockID) {
        clearTimeout(clockID);
        clockID = 0;
    }
}


function checkActivity() {
    $(".onoffswitch").each(function(i, index) {
        var checkbox = $(index).find('.onoffswitch-checkbox')
        var urli = "http://" + $(checkbox).attr('ip') + '/homeAuto/execution/checkstate.php';
        var input = $(checkbox).attr('gpio');
        $.get(urli + '?gpio=' + input, function(data) {
            var checkbox = $(index).find('.onoffswitch-checkbox')
            if (data == 'Off') {

                $(checkbox).removeAttr('checked')
            } else if (data == 'On') {
                $(checkbox).attr('checked', 'true')
            }
        })
    })
}


$(function() {
    /* START OF DEMO JS - NOT NEEDED */
    $.get('ambientes/criarAmbientes.php', function(data) {
        $(".menuambiente").html(data);
        $('.ambiente').on('click', function(event) {
            event.preventDefault();
            $(this).closest('.navbar-minimal').toggleClass('open');
            $(".meioautomatico").load($(this).attr('href'), function() {
                StartClock('d/m/Y', 'H:i:s');
                checkActivity();
            });
        })

        $(".ambiente").each(function(i, index) {
            var ambientepadrao = ($(index).attr('padrao'));
            var localpadrao = ($(index).attr('href'));
            if (ambientepadrao == 'true') {
                $(".meioautomatico").load(localpadrao, function() {
                    StartClock('d/m/Y', 'H:i:s');

                    checkActivity();
                });
            }
        })

    })
    $('#fullscreen').on('click', function(event) {
        event.preventDefault();
        window.parent.location = $('#fullscreen').attr('href');
    });
    $('#fullscreen').tooltip();
    /* END DEMO OF JS */

    $('.navbar-toggler').on('click', function(event) {
        event.preventDefault();
        $(this).closest('.navbar-minimal').toggleClass('open');
    })



})

function changeGPIO(dataComponent, checkbox, type) {
    varcheck = (checkbox.checked);
    mac_dispositivo = dataComponent.replace(/:/g, '');
    
    if (varcheck == false) {
        status = 0;
    } else {
        status = 1;
    }
          var urli = "mosquitto/application/setdata.php";

        $.post(urli,{'status': status , 'component': mac_dispositivo} , function(data){

        });



}