Meu projeto de Automação RESIDENCIAL

* Necessário ter o composer instalado

* Necessário instalar o twig na pasta do projeto (/var/www/html/homeAuto/twig/) (composer require twig/twig:~1.0);

* colocar no visudo a permissão pro apache executar os scripts em python

www-data        ALL=(ALL) NOPASSWD: /var/www/html/homeAuto/execution/checkstate.py, NOPASSWD: /var/www/html/homeAuto/execution/onewire.py

* Foi colocado no header dos arquivos PHP o header('Access-Control-Allow-Origin: *');
porém mais pra frente vou fazer esse Access-Control ser acessível somente pelos ips internos do RASPI

* Dar chmod 777 -R na pasta execution ( /var/www/html/homeAuto/execution)
